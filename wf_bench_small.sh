#!/bin/bash
#source /usr/local/bin/conda.sh
#conda activate igwn-py37
# Figure out how many cores to run on
NPROCS=$1

echo "Running on $NPROCS cores.";

CFG=./wf_bench_small.json
LOG=./wf_bench_small_$NPROCS.log
APP=${PWD}/Generate_wf_MPI.py

mkdir wf_bench_out
mpirun -n ${NPROCS} --use-hwthread-cpus python  ${APP} -o ${CFG} &> ${LOG}
grep "Waveform generation took" wf_bench_small_${NPROCS}.log | sed 's/Waveform generation took --- //' | sed 's/ seconds ---//' > benchmark_${NPROCS}.dat
rm -rf ./wf_bench_out/tmp
mv ./wf_bench_out ./wf_bench_out_$NPROCS

